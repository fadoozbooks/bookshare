<html>
<head>
  <title>Recent Activities</title>
  <?php
    include 'includes/header.php';
    $fadooz = new mysqli('localhost', 'root', 'playbook', 'fadooz');
    $query = "SELECT * from Activity order by TimeStamp DESC";
    $Activities = $fadooz->query($query);
   ?>
</head>

<body>
   <?php
   include 'includes/navbar.php';
   ?>
  <div class="container">
    <div class="row" id="shelf-heading">
      Recent Activities
    </div>
    <div class="row">
      <div class="col-lg-3"></div>
      <div class="col-lg-6">
      &nbsp;
      <?php
        $count = 0;
        while (($Activity = $Activities->fetch_object()) && $count < 10) {
          $count = $count + 1;
          $username = $Activity->username;
          $GoogleID = $Activity->GoogleID;
          $time =  $Activity->TimeStamp;
          $user = getUserInfo($username);
          $book = getBookInfoFromGoogle($GoogleID);
          echo '<div id="row">';
          if ($Activity->Added) {
            echo '<div class="col-lg-12" id="book-info">';
            echo '<div class="row"><p><a href="/user.php?username='.$username.'">'.$user->First_Name.'</a> added <a href="/book.php?id='.$GoogleID.'">'.$book->title.'</a> to his shelf.</p></div>';
            echo '<div class="row"><a href="/book.php?id='.$GoogleID.'"><img src="'.$book->image.'"/></a></div>';
            echo '</div>';
            echo '&nbsp;';
          } else {
            echo '<div class="col-lg-12" id="book-info">';
            echo '<div class="row"><p><a href="/user.php?username='.$username.'">'.$user->First_Name.'</a> deleted <a href="/book.php?id='.$GoogleID.'">'.$book->title.'</a> from his shelf.</p></div>';
            echo '<div class="row"><a href="/book.php?id='.$GoogleID.'"><img src="'.$book->image.'"/></a></div>';
            echo '</div>';
            echo '&nbsp;';
          }
          echo '</div>';
        }
      ?>
      </div>
    </div>
  </div>
</body>
</html>
