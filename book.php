<head>
  <?php
    include 'includes/header.php';
    $id = $_GET['id'];
  
    //Strip everything except alphanumeric characters to avoid syntax errors in mysql
    $table_name = preg_replace("/[^a-zA-Z0-9]+/", "", $id);
  
    system('python id_search.py '.$id);
    system('python transformer.py '.$table_name);
  
    $users = new mysqli('localhost', 'root', 'playbook', 'fadooz');
    //display book details fetched from google
    $conn = new mysqli('localhost', 'root', 'playbook', 'queries');
    if ( $conn->connect_error ) {
      die('Connection failed: '.$conn->connect_error);
    }
    $output = $conn->query('SELECT * from '.$table_name);
    $book = $output->fetch_array();
    $title = $book['title'];
    $authors = $book['authors'];
    $image = $book['image'];
    $description = $book['description'];
    $publisher = $book['publisher'];
  
    $login_status = validate_login_session();
    if ( $login_status) {
      $sql_query = "SELECT * from Books where username='".$_SESSION['username']."' and GoogleID='".$_GET['id']."'";
      $result = $users->query($sql_query);
      $status = $result->fetch_array();
    }
  ?>
  <title>Book description</title>
</head>

<body>
  <?php
    include 'includes/navbar.php';
  ?>
  <div class="container" id="book-page">
   <div class = "col-lg-1"></div>
     <div class="col-lg-10">
      <div class="row">
       <div class="col-lg-2" id="book-image-col">
         <div class="row" id="book-image-row">
         <?php
          echo '<img src = "'.$image.'" class="img-responsive" id="book-image"></div>';
          echo '<div class="row" id="book-image-col-info">';
          if ( $status ) {
            echo '<br><a href="/del_book.php?id='.$_GET['id'].'"><button class="btn btn-danger" id="add-delete-button">Delete from Shelf</button></a>';
          } else {
            echo '<br><a href="/add_book.php?id='.$_GET['id'].'"><button class="btn btn-success" id="add-delete-button">Add to Shelf</button></a>';
          }
          echo '</div>';
         ?>
       </div>
       <div class="col-lg-1"></div>
       <div class="col-lg-9" id="book-info">
         <?php
           echo '<div class="row" id="book-title"><h4>'.$title.'</h4></div>';
           echo '<div class="row" id="book-author"><h5>by '.$authors.'</h5></div>';
           echo '<div class="row" id="book-publisher"><h4>Published by '.$publisher.'</h4></div>';
           echo '<div class="row" id="book-description"><p>'.$description.'</p></div>';
         ?>
      </div>
    </div>
    <div class="row" id="owner-heading">Owners</div>
    <?php
      //display the owners of book on local database
      if ($users->connect_error) {
        die ("Connectin failed: ".$users->connect_error);
      }
      echo '<div class="row" id="all-owners">';
      $output = $users->query("SELECT * from Books where GoogleID='".$id."'");
      while ($result = $output->fetch_object()) {
        $users_query = $users->query("SELECT * from Users where username='".$result->username."'");
        $user = $users_query->fetch_object();
        $name = $user->First_Name.' '.$user->Last_Name;
        echo  '<div class="col-lg-1" id="owner-info">';
        echo    '<img src = "'.$user->profile_picture.'" class="img-responsive" alt="'.$name.'" title="'.$name.'" id="owner-image" />';
        echo  '</div>';
      }
      echo '</div>';

    //Loop for echoing the owners of the book.
    ?>
    </div>
    <div class="col-lg-1"></div>
  </div>
  <?php
    include 'includes/footer.php';
  ?>
</body>
