<!DOCTYPE html>
<?php

session_start();
$conn = new mysqli("localhost", "root", "playbook", "queries");
$query_result = $conn->query("SELECT * FROM ".$_SESSION["table_name"]." WHERE google_id=".$_SESSION["google_id"]."'");
$book_details = $query_result->fetch_object();

if ($conn->connection_error) {
    die("Connection Error!");
}
?>

<html lang="en">
<head>
 <title>book</title>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
</head>
<body>
<div class="container">
 <div class="col-lg-6"><div class="well">
  <div class="row">
  <?php
    echo '<div class="col-lg-4"><img src = "'.$book_details->image.'" class="img-thumbnail" ></div>';
  ?>
    <div class="col-lg-8">
  <?php
    echo '<div class="row"><h4>'.$book_details->title.'</h4></div>';
    echo '<div class="row"><h4>'.$book_details->authors.'</h4></div>';
    echo '<div class="row"><h4>'.$book_details->publisher.'</h4></div>';
  ?>
    </div>
  </div>
 </div></div>
</div>
</body>
</html>
