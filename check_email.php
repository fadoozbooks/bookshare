<?php
$conn = new mysqli("localhost", "root", "playbook", "fadooz");
if(!$conn) {
  die("Connection Error. Please try later.");
}

if (isset($_POST['email'])) {
  $query = $conn->prepare("SELECT * FROM Users WHERE email=?");
  $query->bind_param("s", $_POST['email']);
  $query->execute();
  $result = $query->get_result();
  echo $result->num_rows;
}
?>
