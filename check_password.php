<?php
$conn = new mysqli("localhost", "root", "playbook", "fadooz");
if(!$conn) {
  die("Connection Error. Please try later.");
}

if(isset($_POST["email"])) {
  $query = $conn->prepare("SELECT * FROM Users where email=?");
  $query->bind_param("s", $_POST["email"]);
  $query->execute();
  $pass = $query->get_result();
  if($pass->num_rows == 0) {
    echo 0;
  }
  else {
    $row = $pass->fetch_object();
    if($row->password == $_POST['password']) {
      echo 1;
    }
    else {
      echo 0;
    }
  }
}

$conn->close();
?>
