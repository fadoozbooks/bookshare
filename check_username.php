<?php
$conn = new mysqli("localhost", "root", "playbook", "fadooz");
if(!$conn) {
  die("Connection Error. Please try later.");
}

if (isset($_POST['username'])) {
  $query = $conn->prepare("SELECT * FROM Users WHERE username=?");
  $query->bind_param("s", $_POST['username']);
  $query->execute();
  $result = $query->get_result();
  echo $result->num_rows;
}
?>
