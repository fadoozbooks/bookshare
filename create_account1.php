<!DOCTYPE html>

<html>
<head>
  <title>Book</title>
  <?php
    include 'includes/header.php';
  ?>
  <style type="text/css">
    #signup-button{
      float: right;
    }
  </style>
  <script type='text/javascript'>
  $(document).ready(function() {
    var check_username = false;
    var click_check_username = false;
    $("#username").keyup(function() {
      $("#uname_status").html('');
      $("#uname_div").removeClass("has-success has-error");
      $("#uname_status").removeClass();
    });

    $("#check_avail").click(function()
    {
      var username = $("#username").val();
      var msgbox = $("#status");
      click_check_username = true;
      if(username.length >= 3)
      {
        $.post("check_username.php", {username:username},
          function(result) {
            if(result == 0) {
              check_username = true;
              $('#uname_div').addClass('has-success');
              $("#uname_status").addClass('glyphicon glyphicon-ok form-control-feedback')
            }
            else {
              $('#uname_div').addClass('has-error');
              $("#uname_status").addClass('glyphicon glyphicon-remove form-control-feedback')
              check_username = false;
            }
          }
        );
      }
      else {
          $('#uname_div').addClass('has-error');
          $("#uname_status").addClass('glyphicon glyphicon-remove form-control-feedback');
          check_username = false;
      }
    });

    $("#username").change(function() {
      click_check_username = false;
    });

    var chk_pass_length = false;

    $("#pwd").keyup(function() {
      $("#pass_div").removeClass("has-error");
      $("#pwd_status").removeClass();
      pass = $("#pwd").val();
      if(pass.length >= 4) {
        chk_pass_length = true;
      }
      else {
        chk_pass_length = false;
      }
    });

    $("#chk_pwd").keyup(function() {
      $("#chk_pwd_div").removeClass("has-error");
      $("#chk_pwd_status").removeClass();
    })

    $("#reg_form").submit(function(e) {
      var empty=false;
      var pass_validate=false;
      $("#reg_form input").each(function() {
        if($(this).val() == '') {
          empty = true;
        }
      });

      if(empty) {
        e.preventDefault();
        alert("All fields are mandatory.");
        return;
      }

      if(click_check_username == false) {
          alert("Please click on check availability.");
          e.preventDefault();
          return;
      }

      if(check_username == false) {
        e.preventDefault();
      }

      if(chk_pass_length == false) {
        e.preventDefault();
        $("#pass_div").addClass("has-error");
        $("#pwd_status").addClass("glyphicon glyphicon-remove form-control-feedback");
        return;
      }

      if($("#pwd").val() == $("#chk_pwd").val()) {
          pass_validate = true;
      }
      else {
          pass_validate = false;
      }
     if(!pass_validate) {
        e.preventDefault();
        $("#chk_pwd_div").addClass("has-error");
        $("#chk_pwd_status").addClass("glyphicon glyphicon-remove form-control-feedback");
        return false;
      }
    });
  });
  </script>
</head>


<body>
  <?php
    include 'includes/navbar.php';
  ?>
  <div class="container">
    <h2>Create a new account</h2>
    <form class="form-horizontal" role="form" action='signup.php' method='post' id="reg_form">

      <div class="form-group row">
        <label class="control-label col-sm-2" for="firstname">First Name: </label>
        <div class="col-sm-2"><input type="text" class="form-control" name="firstname" id="firstname"></div>
        <label class="control-label col-sm-2" for="lastname">Last Name: </label>
        <div class="col-sm-2"><input type="text" class="form-control" name="lastname" id="lastname"></div>
      </div>

      <div class="form-group has-feedback">
        <label class="control-label col-sm-2" for="text">Username: </label>
        <div class="col-sm-6"  id="uname_div">
          <input type="text" class="form-control" name="username" id="username" placeholder="Choose a username. Minimum three characters.">
          <span id='uname_status'></span>
        </div>
        <span id='check_avail'><button type="button" class="btn btn-primary btn-xs" id="check_avail">Check Availability</button></span>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2" for="email">Email:</label>
        <div class="col-sm-6"><input type="email" name="email" class="form-control" id="email" placeholder="Enter your e-mail id"></div>
      </div>

      <div class="form-group has-feedback" >
        <label class="control-label col-sm-2" for="password">Password: </label>
        <div class="col-sm-6" id="pass_div">
          <input type="password" class="form-control" name="password" id="pwd" placeholder="Choose a password. Minimum 4 characters.">
          <span id="pwd_status"></span>
        </div>
      </div>

      <div class="form-group has-feedback">
        <label class="control-label col-sm-2" for="password">Confirm Password: </label>
        <div class="col-sm-6" id="chk_pwd_div">
          <input type="password" class="form-control" name="password" id="chk_pwd" placeholder="Re-type password.">
          <span id="chk_pwd_status"></span>
        </div>
      </div>

      <div class="col-sm-offset-2 col-sm-6">
        <button type="submit" class="btn btn-primary" id="signup-button">SignUp</button>
      </div>

    </form>
  </div>
</body>
</html>
