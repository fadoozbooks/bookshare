<html>
<head>
  <?php
    include 'includes/header.php';
    echo "<title>".$_GET['query_title']."</title>";
  ?>
  <style type="text/css">
    #box{
    padding-top: 20px;
    padding-bottom: 20px;
    border-bottom: 1px solid #EFEFEF;
    }
  </style>
</head>

<body>
<?php
  include 'includes/navbar.php';
  //Establish connection to the database
  $conn =  new mysqli("localhost","root","playbook","queries");
  if (mysqli_connect_errno())
    echo 'connection error';
  $result = $conn->query("SELECT * from ".$_SESSION['table_name']);
  if (!$result){
    throw new Exception("Database error [{$conn->error}]");
  }
  echo'<div class="container">';
  echo'<div class="col-xs-0 col-sm-1 col-md-2 col-lg-3"></div>';
  echo'<div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">';
  //check the syntax
  $count = 0;
  while ( $output = $result->fetch_object()  ) {
   echo'<div class="row" id="box">';
	echo'<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    echo'<img src = "'.$output->image.'" height="150px"></div>';
	echo'<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">';
	 echo '<div class="row"><h4><a href="/book.php?id='.$output->google_id.'">'.$output->title.'</a></h4></div>';
	 echo'<div class="row"><h4>'.$output->authors.'</h4></div>';
	 echo'<div class="row"><h4>'.$output->publisher.'</h4></div>';
    echo'</div>';
   echo'</div>';
   $count = $count + 1;
  }
  echo'</div>';
  echo'<div class="col-xs-0 col-sm-1 col-md-2 col-lg-3"></div>';
  echo'</div>';

  echo"\n";
?>
</body>
</html>
