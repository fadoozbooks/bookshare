import requests, json, sys

g_id = sys.argv[1]

res = requests.get("https://www.googleapis.com/books/v1/volumes?q=id:" + g_id)
jsonData = json.loads(res.content)
g_id = ''.join(x for x in g_id if x.isalnum())
file = open("json/"+ g_id + ".json",'w')
file.write(res.content)
file.close()
