<?php
  include 'includes/functions.php';
?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type=text/css href="/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type=text/css href="/css/style.css">

<style type="text/css">
  #search-input{
    width: 185px;
  }
  #search-form{
    padding-top: 8px;
  }
</style>

<script src="/bootstrap/js/bootstrap.min.js"></script>
<script src="/jquery.min.js"></script>
