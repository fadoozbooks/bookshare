<!-- fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top" id="top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="head">
        <form action="/search.php" method="get" class="form-inline" role="form">

          <a class="navbar-brand" href="#" id="site-name">Rack</a>
          <div class="form-group has-feedback" id="search-form">
            <input type="text" class="form-control" id="search-input" name="query_title" placeholder="Search a book" />
          </div>
        </form>
      </div>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="/activity.php">Recent</a></li>
      <?php
      if (!isset($_SESSION['username'])) {
        echo '<li><a href="/login.php"><span class="glyphicon glyphicon-log-in"> Login</span></a></li>';
        echo '<li><a href="/create_account1.php"><span class="glyphicon glyphicon-registration"> Signup</span></a></li>';

      } else {
        echo '<li><a href="/user.php?username='.$_SESSION['username'].'">'.$_SESSION['First_Name'].'</a>';
        echo '<li><a href="/user.php?username='.$_SESSION['username'].'">Your Shelf</a></li>';
        echo '<li><a href="/logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>';
      }
      ?>
      </ul>
    </div>
  </div>
</nav>
