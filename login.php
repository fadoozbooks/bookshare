<html>
<head>
  <?php
    include 'includes/header.php';
  ?>
  <style type="text/css">
    .login-content {
      padding-right: 30px;
      padding-left: 10px;
    }
  </style>
  <script type="text/javascript">
    $(document).ready(function() {
      var email_found = 0;
      var password_match = false;
      var email = $("#email").val();
      var pwd = $("#pwd").val();

      $('#pwd').keyup(function() {
        $('#password-div').removeClass('has-error');
        $('#password-status').removeClass();
      });

      $('#email').keyup(function() {
          $('#email-div').removeClass('has-error');
          $('#email-status').removeClass();
      });

      $('#login-form').submit(function(e) {
        var email = $('#email').val();
        var pwd = $('#pwd').val();

        if($('#email').val() == '') {
          e.preventDefault();
          $('#email-div').addClass('has-error');
          $("#email-status").addClass("glyphicon glyphicon-remove form-control-feedback");
          return;
        }

        if($('#pwd').val() == '') {
          e.preventDefault();
          $('#password-div').addClass('has-error');
          $("#password-status").addClass("glyphicon glyphicon-remove form-control-feedback");
          return;
        }

        e.preventDefault();
        $.post("check_email.php", {email:email}, function(result) {
          if(result != 0) {
            email_found = 1;
          }
          else {
            email_found = 0;
            $('#email-div').addClass('has-error');
            $("#email-status").addClass("glyphicon glyphicon-remove form-control-feedback");
          }
        }).done(function() {
          if(email_found) {
            $.post("check_password.php", {email:email, password:pwd}, function(result) {
              if(result != 0) {
                password_match = true;
              }
              else {
                password_match = false;
                $('#password-div').addClass('has-error');
                $("#password-status").addClass("glyphicon glyphicon-remove form-control-feedback");
              }
            }).done(function() {
              if(password_match) {
                $("#chk").html("something");
                $.post("start_session.php", {email:email}, function(result) {
                  if(result == 'error') {
                    alert("Something happened. Refresh the page and try again.");
                  }
                  else {
                    window.location.href = "user.php?username=" + result;
                  }
                });
              }
            });
          }
        });
      });
    });
  </script>
</head>

<body>
  <?php
  include 'includes/navbar.php';
  ?>
  <div class="container-fluid" id="login-page-container">
    <div class="col-md-offset-4 col-lg-offset-4 col-sm-offset-2 col-md-4 col-sm-8 col-lg-4 login-box">
      <div class="login-content">

        <div class="row">
          <h3 style="text-align: center;" id="chk">Login</h3>
        </div>

        <div class="row">
          <form class="form-horizontal" role="form" method="post" id="login-form">

            <div class="form-group has-feedback">
              <label for="username" class="control-label col-md-3">Email: </label>
              <div class="col-md-9" id="email-div">
                <input class="form-control" type="email" name="email" id="email" placeholder="Enter your registered email ID">
                <span id="email-status"></span>
              </div>
            </div>

            <div class="form-group has-feedback">
              <label class="control-label col-md-3">Password: </label>
              <div class="col-md-9" id="password-div">
                <input class="form-control" type="password" name="password" id="pwd" placeholder="Enter your password">
                <span id="password-status"></span>
              </div>
            </div>

            <div class="row">
              <div class="col-md-offset-3 col-md-5">
                <a href="forgot_password.php">forgot password?</a>
              </div>
              <div class="col-md-4">
                <button type="submit" class="btn btn-primary" id="submit-button" style="float: right;">Submit</button>
              </div>
            </div>

          </form>
        </div>

      </div>
    </div>
  </div>

<?php
include 'includes/footer.php';
?>
