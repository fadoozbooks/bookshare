<?php

  $query = htmlspecialchars($_GET['query_title']);
  $search_query = '"'.$query.'"';
  $query = preg_replace("/[^a-zA-Z0-9]+/", "", $query);

  if (empty($query)) {
  	echo 'Please enter a query, an author name, or book name or something!';
  }
  else {
	  session_start();
	  $_SESSION['table_name'] = $query;
	  $table_name = $query;
	  system("python title_search.py ".$search_query, $jsonFile);
    system("python transformer.py ".$query, $table_name);
    $_SESSION['table_name'] = $query;
    include "display_search_results.php";
	}
?>
