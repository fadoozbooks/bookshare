<?php
$conn = new mysqli("localhost", "root", "playbook", "fadooz");
if(!$conn) {
    die("Error: Please try again..");
}

$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$email = $_POST['email'];
$password = $_POST['password'];
$username = $_POST['username'];

$check_email_query = $conn->prepare("SELECT email FROM Users WHERE email=?");
$check_email_query->bind_param("s", $email);
$check_email_query->execute();
$result = $check_email_query->get_result();
if($result->num_rows > 0) {
  header("refresh: 3; url=create_account1.html");
  echo "<h3>Email address already registered</h3>";
  echo "<p>You will be redirected to signup page automatically. If not, click <a href='create_account1.html'>here</a></p>";
  exit;
}

$sql_query = $conn->prepare("INSERT INTO Users(First_Name, Last_Name, email, username, password)
              Values(?, ?, ?, ?, ?)");
              
$sql_query->bind_param("sssss", $firstname, $lastname, $email, $username, $password);

if($sql_query->execute()) {
    header("refresh: 3; url=login.php");
    echo '<h3>Account created succesfully.</h3>';
    echo '<p>You will be redirected to login page automatically. If not click <a href="login.php" title="login" alt="login">here</a></p>';
}
else {
    echo 'Something bad happened. Please try later. Click this <a href="create_account1.html">link</a> to try again.';
}
?>
