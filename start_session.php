<?php
  $link = new mysqli("localhost", "root", "playbook", "fadooz");
  if(!$link) {
    echo 'error';
  }
  else {
    session_start();
    $query = $link->prepare("SELECT * FROM Users WHERE email=?");
    $query->bind_param("s", $_POST['email']);
    $query->execute();
    $result = $query->get_result();
    $row = $result->fetch_object();
    $_SESSION['email'] = $row->email;
    $_SESSION['password'] = $row->password;
    $_SESSION['username'] = $row->username;
    $_SESSION['logged_in'] = 1;
    echo $row->username;
  }
?>
