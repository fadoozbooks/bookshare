import requests, json, sys

title = sys.argv[1]

res = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + title)
jsonData = json.loads(res.content)

file_name = ''.join(x for x in title if x.isalnum())
file = open("json/"+ file_name + ".json",'w')
file.write(res.content)
file.close()
