import json, sys
import os
import MySQLdb
import string


# decodes json data for a single book
# returns a dictionary with book info
def decodeJsonBookData(book_data):
    volumeInfo = book_data['volumeInfo']
    bookInfo = dict()

    bookInfo['id'] = book_data['id']
    bookInfo['title'] = volumeInfo.get('title').encode('ascii', 'ignore')

    # As google returns authors list, combine them to string
    t_authors = volumeInfo.get('authors', ' ')
    bookInfo['authors'] = ', '.join(t_authors).encode('ascii', 'ignore')

    # Some books details don't have image links
    if volumeInfo.get('imageLinks') != None:
        bookInfo['image'] = volumeInfo['imageLinks']['thumbnail']
    else:
        bookInfo['image'] = None

    if volumeInfo.get('pageCount') != None:
        bookInfo['page_count'] = int(volumeInfo.get('pageCount'))
    else:
        bookInfo['page_count'] = 0

    bookInfo['publisher'] = volumeInfo.get('publisher', ' ').encode('ascii', 'ignore')
    bookInfo['text_snippet'] = volumeInfo.get('textSnippet', ' ').encode('ascii', 'ignore')
    bookInfo['rating'] = volumeInfo.get('rating', ' ').encode('ascii', 'ignore')
    bookInfo['description'] = volumeInfo.get('description', ' ').encode('ascii', 'ignore')

    # ISBN Details

    return bookInfo


# stores data into database
def storeData(book_info, cur, table_name):
    g_id = book_info['id']
    title = book_info['title']
    authors = book_info['authors']
    image = book_info['image']
    page_count = book_info['page_count']
    publisher = book_info['publisher']
    text_snippet = book_info['text_snippet']
    #rating = book_info['rating']
    description = book_info['description']

    #Need to replace every single quotes with two of them.

    description = string.join(description.split("'"), "''")
    #print type(rating)
    t_rating = 5.0

    sql = '''
        INSERT INTO %s(google_id, title, authors, image, page_count,
        publisher, text_snippet, description)
        VALUES("%s", "%s", "%s", "%s", %d, "%s", "%s", '%s')
        ''' % (table_name, g_id, title, authors, image, page_count,
        publisher, text_snippet, description)
    cur.execute(sql)


# main function
def main(jsonFile):
    #connect to mysql database
    queries = MySQLdb.connect('localhost', 'root', 'playbook', 'queries')
    cursor = queries.cursor()
    temp_filename = jsonFile.split('.')
    table_name = temp_filename[0]
    sql = "DROP TABLE IF EXISTS %s" % table_name
    cursor.execute(sql)

    #create a table
    sql = '''CREATE TABLE %s(
		   serial_id INT PRIMARY KEY AUTO_INCREMENT,
           google_id VARCHAR(40),
           title VARCHAR(100),
           authors VARCHAR(100),
		   image VARCHAR(255),
           page_count INT,
           publisher VARCHAR(100),
           text_snippet VARCHAR(255),
           description TEXT
           )''' % table_name
    cursor.execute(sql)

#    jsonData = getDataFromGoogle(jsonFile)
    jsonData = json.load(open('json/'+jsonFile+'.json'))
    count = 1;
    for book in jsonData['items']:
        book_info = decodeJsonBookData(book)
        storeData(book_info, cursor, table_name);

    queries.commit()
    queries.close()
    os.system('rm -f json/'+jsonFile+'.json')


if __name__ == "__main__":
    jsonFile = sys.argv[1]
    main(jsonFile)
