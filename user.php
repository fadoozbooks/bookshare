<html>
<head>
  <?php
    include 'includes/header.php';
    
    if ($_GET['username'] == NULL) {
      header("Location:/  ");
    } else {
      $username = $_GET['username'];
    }
    session_start();
    echo "<title>".$username."'s shelf</title>";
    $conn = new mysqli('localhost', 'root', 'playbook', 'fadooz');
    if ( $conn->connect_error ) {
      die('Connection failed: '.$conn->connect_error);
    }
    $sql_query = $conn->query("SELECT * from Users where username='".$username."'");
    $user = $sql_query->fetch_object();

    $shelf = $conn->query("SELECT * from Books where username='".$username."'");
  ?>
</head>

<body>
  <?php
  include 'includes/navbar.php';
  ?>
  <div class="container">
    <div class="col-lg-1"></div>

    <div class="col-lg-10">
      <div class="row" id="shelf-heading">
          <?php
              if ($_SESSION['username'] == $username)
                  echo "Your Shelf";
              else
                  echo $user->First_Name."'s Shelf";
          ?>
      </div>
      <div class="col-lg-1"></div>
      <div class="col-lg-8" id="shelf-column">
        <div class="row" id="add-a-book">
        <?php
          if ($_SESSION['username'] == $_GET['username'] && $_SESSION['password'] == $user->password){
            include 'includes/add_book.html';
          }
        ?>
        <br /><br />
        </div>
        <div class="row" id="all-books">
        <?php while ($book = $shelf->fetch_object()){
          echo '<div class="col-lg-3" id="shelf-image">';
          echo '<a href="/book.php?id='.$book->GoogleID.'"><img src="'.$book->image.'" id="book-shelf-image"></a>';
          echo "\n";
          echo '<p id="book-name">';
          echo '<div id="title"><big>'.$book->title.'</big></div><br>';
          echo '<div id="author">'.$book->author.'</div>';
          echo "</p>";
          echo '</div>'; }
        ?>
        </div>

      <div class="row">
          <div class="col-lg-3" id="personal-col">
              <div class="row" id="personal-info">
                  <img src = "unnamed.jpg" class="img-responsive" id="personal-image">
                  <h4><big><?php echo $user->First_Name; echo ' '; echo $user->Last_Name; ?></big></h4>
                  <br><br>
                  Location
                  <br><br>
                  E-mail
              </div>
          </div>
          <div class="col-lg-9">
              <div class="row" id="add-a-book">
                  <?php
                      if ($_SESSION['username'] == $_GET['username'] && $_SESSION['password'] == $user->password){
                          include 'includes/add_book.html';
                      }
                  ?>
              </div>
              <div class="row">
                  <div class="col-lg-1"></div>
                  <div class="col-lg-11" id="shelf-column">
                      <?php while ($book = $shelf->fetch_object()){
                          echo '<div class="col-lg-3" id="book-shelf-info">';
                          echo '<a href="/book.php?id='.$book->GoogleID.'"><img src="'.$book->image.'" class="img-responsive" id="book-shelf-image"></a>';
                          echo "\n";
                              echo '<p id="shelf-title"><big>'.$book->title.'</big></p><br>';
                              echo '<p id="shelf-author">'.$book->author.'</p>';
                          echo "</p>";
                          echo '</div>'; }
                      ?>
                  </div>
              </div>
          </div>
      </div>
    </div>
    <div class="col-lg-1"></div>
  </div>
  <?php
    include 'includes/footer.php';
  ?>
</body>
</html>
